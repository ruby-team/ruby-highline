ruby-highline (3.1.2-1) unstable; urgency=medium

  * New upstream version 3.1.2

 -- Lucas Nussbaum <lucas@debian.org>  Tue, 28 Jan 2025 23:00:49 +0100

ruby-highline (3.0.1-1) unstable; urgency=medium

  * New upstream version 3.0.1

 -- Lucas Nussbaum <lucas@debian.org>  Sun, 03 Mar 2024 14:44:36 +0100

ruby-highline (2.1.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

  [ Lucas Nussbaum ]
  * New upstream version 2.1.0
  * Clean coverage/ after build. Closes: #1047090
  * debian/copyright: Update Source URL

 -- Lucas Nussbaum <lucas@debian.org>  Sat, 26 Aug 2023 17:04:26 +0200

ruby-highline (2.0.3-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.

  [ Cédric Boutillier ]
  * Bump Standards-Version to 4.5.1 (no changes needed)
  * Bump debhelper compatibility level to 13
  * Skip test failing when ruby is built with libedit instead of readline

 -- Cédric Boutillier <boutil@debian.org>  Thu, 11 Feb 2021 10:52:50 +0100

ruby-highline (2.0.3-1) unstable; urgency=medium

  * Team upload

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Antonio Terceiro ]
  * debian/watch: point at github repository to get tests
  * New upstream version 2.0.3
  * Refresh packaging with a new run of dh-make-ruby
  * Drop existing patch, fixed upstream
  * debian/copyright: drop paragraph about file removed upstream

 -- Antonio Terceiro <terceiro@debian.org>  Sun, 23 Feb 2020 13:28:54 -0300

ruby-highline (1.7.8-1) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Christian Hofstaedtler ]
  * New upstream version, refreshed local patches.

 -- Christian Hofstaedtler <zeha@debian.org>  Thu, 05 May 2016 23:05:51 +0000

ruby-highline (1.7.2-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * disable_test_terminal_size.patch: refresh
  * Update packaging with a new dh-make-ruby run

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 20 Jun 2015 21:00:52 -0300

ruby-highline (1.6.21-1) unstable; urgency=medium

  * Imported Upstream version 1.6.21
  * Build with newer gem2deb; installing rubygems metadata in a Ruby version
    independent directory.

 -- Cédric Boutillier <boutil@debian.org>  Wed, 07 May 2014 23:36:20 +0200

ruby-highline (1.6.20-1) unstable; urgency=medium

  * Imported Upstream version 1.6.20
  * Bump Standards-Version to 3.9.5 (no changes needed)
  * Refresh disable_test_terminal_size.patch

 -- Cédric Boutillier <boutil@debian.org>  Fri, 10 Jan 2014 08:03:00 +0100

ruby-highline (1.6.19-1) unstable; urgency=low

  [ Prach Pongpanich ]
  * Imported Upstream version 1.6.19
  * Bump Standards-Version to 3.9.4 (no changes needed)
  * Drop transitional binary packages

  [ Cédric Boutillier ]
  * debian/control:
    + remove obsolete DM-Upload-Allowed flag
    + use canonical URI in Vcs-* fields
    + improve description
    + update my email address
  * set external encoding when needed for tests
  * update my email address and years in debian/copyright
  * remove lintian-overrides, about transitional packages
  * add disable_test_terminal_size.patch: disable a failing test with
    pbuilder/sbuild.

 -- Cédric Boutillier <boutil@debian.org>  Fri, 28 Jun 2013 08:20:32 +0200

ruby-highline (1.6.13-2) unstable; urgency=low

  * Added myself to uploaders.
  * Build depend on gem2deb >= 0.3.0.
  * debian/copyright: change reference from common-licenses symlink LPGL to
    LGPL-2.1.
    - Fix lintian warning.

 -- Per Andersson <avtobiff@gmail.com>  Tue, 26 Jun 2012 00:18:32 +0200

ruby-highline (1.6.13-1) unstable; urgency=low

  * New upstream version
  * Drop patches (merged upstream)

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Thu, 21 Jun 2012 00:05:09 +0200

ruby-highline (1.6.12-1) unstable; urgency=low

  * New upstream version
  * Add myself to Uploaders:
  * Bump Standards-Version: to 3.9.3 (no changes needed)
  * Use Breaks instead of Conflicts for transitional packages
  * Set Priority of transitional packages to extra
  * Convert copyright file to DEP-5
  * Override lintian warning about duplicate description for transitional
    packages
  * debian/patches:
    + update remove-shebangs.patch to include new files
    + add change-shebangs-in-examples.patch fixing shebangs in examples

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Wed, 16 May 2012 15:09:24 +0200

ruby-highline (1.6.2-1) unstable; urgency=low

  * New upstream version.
  * Switch to gem2deb-based packaging.

 -- Lucas Nussbaum <lucas@debian.org>  Sun, 05 Jun 2011 09:27:01 +0200

libhighline-ruby (1.5.2-1) unstable; urgency=low

  [ Ryan Niebur ]
  * set myself as Maintainer

  [ Paul van Tilburg ]
  * New upstream release.
  * Added support for Ruby 1.9.1.
  * debian/control:
    - Bumped standards version to 3.8.4.
    - Bumped debhelper build-depend to >= 5.
    - Added a libhighline-ruby1.9.1 package entry.
    - Added a build depend on ruby1.9.1.

 -- Paul van Tilburg <paulvt@debian.org>  Sun, 14 Feb 2010 17:51:37 +0100

libhighline-ruby (1.5.1-1) unstable; urgency=low

  * New upstream release
  * add myself to uploaders
  * refresh patches
  * add Homepage field
  * add README.source

 -- Ryan Niebur <ryanryan52@gmail.com>  Sun, 10 May 2009 12:11:54 -0700

libhighline-ruby (1.5.0-1) unstable; urgency=low

  [ Gunnar Wolf ]
  * Changed section to Ruby as per ftp-masters' request

  [ Paul van Tilburg ]
  * New upstream release.
  * debian/control:
    - Bumped the standards version to 3.8.1.
    - Added a depend on ${misc:Depends} to libhighline-ruby{,-doc,.1.8}.
  * debian/compat: bumped compatibility to version 5.
  * debian/copyright: converted to the UTF-8 charset.
  * debian/patches: updated 01_fix-shebang.patch.

 -- Paul van Tilburg <paulvt@debian.org>  Tue, 05 May 2009 19:47:17 +0200

libhighline-ruby (1.4.0-1) unstable; urgency=low

  * New upstream release.
  * Adapted debian/control, debian/rules, and remove debian/control.in to
    drop the Uploaders rule.
  * Added myself to the Uploaders field.

 -- Paul van Tilburg <paulvt@debian.org>  Tue, 19 Jun 2007 11:05:20 +0100

libhighline-ruby (1.2.2-1) unstable; urgency=low

  * New upstream release
  * Patch yet another file (new in this release) to have the correct ruby path
  * Add "ruby1.8" to libhighline-ruby1.8 Depends:

 -- Esteban Manchado Velázquez <zoso@debian.org>  Sat, 21 Oct 2006 20:37:12 +0100

libhighline-ruby (1.2.0-1) unstable; urgency=low

  * First upload (Closes: #372530).

 -- Esteban Manchado Velázquez <zoso@debian.org>  Sat, 10 Jun 2006 15:23:13 +0100
